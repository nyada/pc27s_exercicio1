/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class NataliaPrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private  int ID=0;
    public NataliaPrintTasks(String name, int ID){
        taskName = name;
        
        //Tempo aleatorio entre 0 e 1 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
        
        this.ID = ID;
    }
    
    public int getID(){
        return this.ID;
    }
    
    @Override
    public void run(){
        try{
            System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            if (this.ID % 2 != 0)
            Thread.sleep(sleepTime);
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
