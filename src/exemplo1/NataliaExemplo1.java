/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Lucio
 */
public class NataliaExemplo1  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");
            
            ExecutorService obj = Executors.newCachedThreadPool();
            
            
            //Cria cada thread com um novo runnable selecionado
            Thread t;
            for (int i=0; i<20; i++){
                t= new Thread(new NataliaPrintTasks("thread"+i, i));
                //Inicia as thread e as coloca no estado executavel
                obj.execute(t);
            }
      
            
            System.out.println("Threads criadas");
        }
        
}


        
